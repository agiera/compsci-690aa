# The greedy 2-approximation of k-centers


To save an instance of k-centers:
store an adjacency matrix and k value in a matlab instance
run the save(filename : string, adj_var_name : string, k_var_name : string) function

example:
D = [[0, 10, 7, 6]; [10, 0, 8, 5]; [7, 8, 0, 12]; [6, 5, 12, 0]]
k = 2
save('k-centers_instance.dat', 'D', 'k')


To run an instance file:
run k_centers_greedy_from_file(filename : string)

example:
k_centers_greedy_from_file('k-centers_instance.dat')
