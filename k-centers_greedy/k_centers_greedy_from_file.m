function [ centers ] = k_centers_greedy_from_file( f )
  data = importdata(f);
  centers = k_centers_greedy(data.D, data.k);
end

function [ centers ] = k_centers_greedy( D, k )
%K_CENTER_GREEDY is the greed algorithm for the k center problem
%   D is the adjacency matrix of the graph
%   k is an positive integer
%   centers is a k vector such that centers(i) = 1 iff i is a node in the
%       solution

    centers = zeros(k, 1);
    centers(1) = 1;
    dists = D(1, :);
    for i = 2:k
        % find furthest vertex
        [~, centers(i)] = max(dists);
        % calculate distances from closest center
        dists = min(D(:, centers(1:i)), [], 2);
    end
end
