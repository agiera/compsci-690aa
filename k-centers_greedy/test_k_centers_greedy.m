file_name = 'random_k_centers.dat';
% parameters for random problem generation
np = 10;
k = 6;
wp = 5;
m = 5;

for i = 1:100
    n = round(10*rand(1) + np);
    D = metric_adj(n);
    w = wp .* rand(1, m);
    save(file_name, 'D', 'k');
    centers = k_centers_greedy_from_file(file_name);
    cost = max(min(D(:, centers), [], 2));
    opt_centers = k_centers_brute_force(D, k);
    opt_cost = max(min(D(:, opt_centers), [], 2));
    assert(cost <= 2*opt_cost);
end

function [ D ] = metric_adj( n )
    r = 15;
    dp = 1;
    D = r*rand(n, n) + dp;
    % makes D symmetric
    D = D - tril(D, -1) + triu(D, 1)';
    D = round(D .* (ones(n, n) - eye(n, n)));
    for i = 1:n
        for j = 1:n
            for k = 1:n
                if D(i, j) + D(j, k) < D(i, k)
                    D(i, k) = D(i, j) + D(j, k);
                end
            end
        end
    end
end

function [ best_centers ] = k_centers_brute_force( D, k )
    [n, ~] = size(D);
    
    best_cost = max(min(D(:, ones(k)), [], 2));
    best_centers = ones(k);
    for centers = nchoosek(1:n, k).'
        cost = max(min(D(:, centers), [], 2));
        if cost < best_cost
            best_cost = cost;
            best_centers = centers;
        end
    end
end
