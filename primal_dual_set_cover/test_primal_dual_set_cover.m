file_name = 'random_set_cover.dat';
p = 0.15;
np = 100;
mp = 50;
wp = 10;

k = 50;
while k > 0
    n = round(100*rand(1) + np);
    m = round(50*rand(1) + mp);
    S = rand(m, n) < p;
    % if an element does not appear in a set
    % this is not a valid set cover instance so try again
    if any(sum(S) < 1)
        continue;
    else
        k = k - 1;
    end
    w = ceil(wp .* rand(1, m));
    save(file_name, 'S', 'w');
    x = primal_dual_set_cover_from_file(file_name);
    cost = w * x;
    opt_cost = set_cover_lp_relaxation(S, w);
    % f is the most amount of times an element appears in a set
    f = max(sum(S.', 1));
    assert(cost <= f*opt_cost);
end

function [ opt_cost ] = set_cover_lp_relaxation( S, w )
%SET_COVER_LP_RELATION returns the cost of optimal LP set cover
%   S is an m x n matrix where S(i, j) = 1 iff e_j \in S_i
%   w is an m vector such that w(i) = w(S_i)
%   cost is the cost of the optimal LP solution

  [n, m] = size(S.');
  b = ones(n, 1);
  lb = zeros(m, 1);
  x = linprog(w, -S.', -b, zeros(n, m), zeros(n, 1), lb, Inf(m, 1));
  opt_cost = w * x;
end
