function [ sets ] = primal_dual_set_cover_from_file( f )
  data = importdata(f);
  sets = primal_dual_set_cover(data.S, data.w);
end

function [ idx_vec ] = primal_dual_set_cover( S, w )
%SET_COVER_DUAL is an f-approx of set cover
%   S is an m x n matrix where S(i, j) = 1 iff e_j \in S_i
%   w is an m vector such that w(i) = w(S_i)
%   idx_vec is an m vector such that idx_vec(i) = 1 if S_i belongs
%       to the dual LP rounding solution else idx_vec(i) = 0
%   f is the most amount of times an element appears in a set

  [n, m] = size(S.');
  y = zeros(n, 1);
  idx_vec = zeros(m, 1);
  contains = double(S);
  % contains(i, j) = NaN where e_j \not\in S_i
  contains(contains == 0) = NaN;
  % (S.'*idx_vec)(j) is the number of times e_j occurs in the sets
  % added to the solution
  % so the loop will terminate iff idx_vec is a set cover
  while any(S.'*idx_vec < 1)
      % the jth element of (w.' - S*y) is the slack of set j
      % the jth element of elt_slacks is the slack of e_j
      slacks = contains .* (w.' - S*y);
      [elt_slacks, ~] = min(slacks);
      [m, j] = max(elt_slacks);
      y(j) = y(j) + m;
      tight = slacks(:, j) == m;
      idx_vec(tight) = 1;
  end
end
