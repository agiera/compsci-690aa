# The primal-dual f-approximation for the set cover problem

To save an instance of set cover:
store a matrix describing the sets and weights vector in a matlab instance
run the save(filename : string, sets_var_name : string, w_var_name : string) function

example:
S = [[1, 0, 1, 1, 0]; [0, 1, 0, 0, 1]; [1, 1, 1, 1, 0]]
w = [5, 10, 3]
save('set_cover_instance.dat', 'S', 'w')


To run an instance file:
run primal_dual_set_cover_from_file(filename : string)

example:
primal_dual_set_cover_from_file('set_cover_instance.dat')
